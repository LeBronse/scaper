#!/usr/bin/python3

# Takes in csv files containing box scores from games and
# groups all player stats into one csv file.

import csv
import glob

FIELD_NAMES = ["player","date","mp","fg","fga","fg%","3p","3pa","3p%","ft","fta","ft%","ord","drb","trb","ast","stl","blk","tov","pf","pts","+/-"]
SAVE_FILE_NAME = "combined_player_data.player.csv"

def main():
    players = get_player_stats()

    write_stats_to_file(players)

    combine_player_data(players)


def combine_player_data(players):
    with open (SAVE_FILE_NAME, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=FIELD_NAMES)
        writer.writeheader()

        for player_name, stats in players.items():
            for game in stats:
                writer.writerow(game)


def write_stats_to_file(players):
    for player_name, stats in players.items():
        with open (player_name+".player.csv", 'w') as f:
            writer = csv.DictWriter(f, fieldnames=FIELD_NAMES)
            writer.writeheader()
            for game in stats:
                writer.writerow(game)


def get_player_stats():
    players = {}

    for filename in glob.glob("*.csv"):
        date = filename[:-8]

        with open (filename, 'r', newline='') as box_score_data:
            reader = csv.DictReader(box_score_data)

            for row in reader:
                row["date"] = "{:s}-{:s}-{:s}".format(date[:4], date[4:6], date[6:8])

                try:
                    player = players[row["player"]]
                except KeyError:
                    player = players[row["player"]] = []

                player.append(row)

    return players


main()
