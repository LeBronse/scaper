#!/usr/bin/python3

# Uses basketball-reference.com to scrape box scores from games.
# User inputs the date from which they would like they data pulled from,
# the script will then pull all box score data from that date until today.
# The data is saved into csv files named after the date of the match and
# the team.

import requests
import urllib.request
import sys
from bs4 import BeautifulSoup
from datetime import date, timedelta

# The box score links have this unique text
KEYWORD = "Box Score"
BASE_URL = "https://www.basketball-reference.com"
# The page that the box score links will pulled from, date is dynamically inserted
GAMEDAY_URL = "/boxscores/?month={:d}&day={:d}&year={:d}"
TABLE_NAME = "box-{:s}-game-basic"
TABLE_HEADER = "player,mp,fg,fga,fg%,3p,3pa,3p%,ft,fta,ft%,ord,drb,trb,ast,stl,blk,tov,pf,pts,+/-"
NO_LINK_ERROR = "Couldn't find any links"


def main():
    (day, month, year) = (int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
    box_score_links = (get_box_score_links_from_date(date(year, month, day)))

    if not box_score_links:
        print(NO_LINK_ERROR)
        sys.exit()

    get_box_score_data(box_score_links)


# list[tuple(name, list)]
def get_box_score_data(box_score_links):
    for link in box_score_links:
        matchup_date = link[11:-8]
        response = requests.get(BASE_URL+link)
        soup = BeautifulSoup(response.text, "html.parser")
        tables = soup.find_all('table',{'class':'stats_table'})
        box_score_tables = find_box_score_tables(soup)

        for table in box_score_tables:
            player_stats = extract_player_data_from_table(table)
            table_team_name = table.get('id')[4:-11]

            write_to_file(matchup_date + table_team_name + ".csv", player_stats)


def write_to_file(file_name, player_stats):
    with open(file_name, 'w') as f:
        f.write(TABLE_HEADER+"\n")

        for player in player_stats[1:]:
            f.write(player+"\n")


def find_box_score_tables(soup):
    team_box_score_data = []
    tables = soup.find_all('table',{'class':'stats_table'})

    for table in tables:
        table_team_name = table.get('id')[4:-11]

        # make sure it is the correct table with basic data
        if TABLE_NAME.format(table_team_name) != table.get('id'):
            continue

        team_box_score_data.append(table)

    return team_box_score_data


def extract_player_data_from_table(table):
    player_stats = []

    for row in table.find_all('tr'):
        csv_row = []
        row_text = row.get_text()

        if "Starters" in row_text or "Reserves" in row_text or "Team Totals" in row_text:
            continue

        csv_row.append(row.find('th').get_text())

        for cell in row.find_all('td'):
            cell_text = cell.get_text()
            csv_row.append(cell_text)

        player_stats.append(",".join(csv_row))

    return player_stats


# Get box score links starting from the given date
def get_box_score_links_from_date(start_date):
    current_date = date.today()
    box_scores = []

    # end recursion
    if start_date > current_date:
        return box_scores

    response = requests.get(BASE_URL + GAMEDAY_URL.format(
        start_date.month, start_date.day, start_date.year))

    soup = BeautifulSoup(response.text, "html.parser")

    box_scores = find_box_score_links(soup)

    return box_scores + (get_box_score_links_from_date(
        start_date + timedelta(days=1)))


# filter out all links that aren't for box scores
def find_box_score_links(soup):
    box_scores = []

    for line in soup.find_all('a'):
        if KEYWORD in line:
            box_scores.append(line.get('href'))

    return box_scores


main()
